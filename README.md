# master-project

## Product backlog

Within agile project management, product backlog refers to a prioritized list of functionality which a product should contain. 
A User Story is the most widely used Agile Practice for capturing needs and requirements and describing Product Backlog Items:

https://gitlab.com/BenOrcha/master-project/-/issues?sort=relative_position&state=opened

Notice that Gitlab uses Issues instead of User Stories.

A backlog can contain technical stories which are activitivies needed to be schedulle but for the project team only: https://gitlab.com/BenOrcha/master-project/-/issues/3

### Validation criterions for stories

In order to demonstrate to your customer that a story is achieved, you must add validation criterions to each story: https://gitlab.com/BenOrcha/master-project/-/issues/2#note_881962328

Validation criterions are sentences begining by verify that.

## Defining Sprints

A sprint is a repeatable fixed time-box during which a "Done" product of the highest possible value is created. It is a kind of sub-project belonging to a project.

https://gitlab.com/BenOrcha/master-project/-/milestones

Notice how Gitlab defines milestones instead which can be deadlines for Sprints.

The key point for Sprint is all the sprints must have the same duration (in order to compare what will be done in each sprint).

## Project planning

How to estimate how long will be the project?

- Give points to each story (a point as no unity, it is just a level of difficulty for the achievement of a user story from the team point of view)
- Calculate the sum of points for the entire backlog
- Estimate the initial capacity of the team (as a team how many points are we able to make in the first Sprint?)
- An estimation of the number of sprints in given by = total number of points / intial capacity

Example: points have been added at the end of each story (https://gitlab.com/BenOrcha/master-project/-/issues/2). The sum is 30 points. If the initial capacity is 15, 2 sprints are needed (30/2).

Then distribute evenly the stories into the sprints (no more than the capacity in each sprint).

Example: the 2 firsts stories are in the sprint 1 (for a maximum of 15 points), while the last story (15 points is in sprint 2).

## Sprint planning

At he begining of each sprint, divide the stories from the next sprint (and only the next sprint) into tasks.

Why should I divide into tasks only the stories for the next sprint? Beacause during the spring review (schedulled at the end of each sprint) the content of the future sprints can change accordingly to the agile principle.

Example of a tesk: https://gitlab.com/BenOrcha/master-project/-/issues/4

Notice that:
- The task has an asignee (team member responsible ot this task)
- A label is associated to each task ("user sroty 1" as it happens)in order to link the task to the user story (otherwise there is no way to differenciate a taskto a story)

## Project management

To follow the project progress a kanban view of the project is very useful. Gitlab offers the board view: https://gitlab.com/BenOrcha/master-project/-/boards

See how two columns have been added to the default board containing only open and closed.

You can move all the cards from open to closed and when closed is reached the project progresses (since a task is closed the project is completed at 16%).

## Sprint review

At the end of each sprint.

Attendees include the Scrum Team and key stakeholders invited by the Product Owner;
- The product owner explains what Product Backlog items have been “Done” and what has not been “Done”;
- The development team discusses what went well during the Sprint, what problems it ran into, and how those problems were solved;
- The Development Team demonstrates the work that it has “Done” and answers questions about the Increment;
- The Product Owner discusses the Product Backlog as it stands. He or she projects likely target and delivery dates based on progress to date (if needed);
- The entire group collaborates on what to do next, so that the Sprint Review provides valuable input to subsequent Sprint planning;
- Review of how the marketplace or potential use of the product might have changed what is the most valuable thing to do next; and,
- Review of the timeline, budget, potential capabilities, and marketplace for the next anticipated releases of functionality and capability of the product.

The result of the Sprint Review is a revised Product Backlog that defines the probable Product Backlog items for the next Sprint. The Product Backlog may also be adjusted overall to meet new opportunities.








